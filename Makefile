CXX=g++
CXXFLAGS=-Wall -std=c++14 -O2 -g
LDFLAGS=-lboost_system -lboost_program_options -lpthread
EXEC=ircd-simple
SRC=config.cpp channel.cpp user.cpp session.cpp server.cpp parser.cpp mainframe.cpp main.cpp
OBJS=$(SRC:.cpp=.o)

all: $(EXEC)

ircd-simple: $(OBJS)
	$(CXX) -o $@ $^ $(LDFLAGS)


%.o: %.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS)

clean:
	rm -rf *.o

binclean:
	rm -rf $(EXEC)

distclean: clean binclean

install: all
	mkdir -p $(DESTDIR)/usr
	mkdir -p $(DESTDIR)/usr/bin
	mv $(EXEC) $(DESTDIR)/usr/bin/
