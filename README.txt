======== ircd-simple ========
Original Authors:
	Boutter Loïc
	Masson Guillaume

Updated by:
	Michael Marley

======== Configuration ========
The default network name is "ircd-simple".  This may be changed by passing the command line argument "--networkname <name>".
The default server password is "" (no password).  This may be changed by passing the command line argument "--serverpassword <password>".
The default port is 6667. This may be changed by passing the command line argument "--port <port>".
Debugging output may be enabled by passing the command line argument "--debug".
The default listening address is 127.0.0.1 (connections accepted only from localhost). This may be changed in "server.cpp".

======== Description ========
This is a very simple IRC server designed to be used for private installations where no access control or services
are required.  It is ideal to combine with Quassel or a bouncer to provide a local IRC network without the overhead
of connecting to services like Freenode.

It only supports the most basic parts of the IRC protocol (NICK, USER, QUIT, JOIN, PART, LIST, PRIVMSG, NOTICE, and PING).
There are no modes or operators.

