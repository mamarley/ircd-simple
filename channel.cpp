#include "channel.h"
#include "mainframe.h"
#include "session.h"

#include <iostream>

Channel::Channel(User* creator,const std::string& name)
:	mName(name),
	mUsers(){
		mUsers.insert(creator);
}

Channel::~Channel(){
	
}

std::string Channel::getName() const{
	return mName;
}

void Channel::addUser(User* user){
	if(user){
		mUsers.insert(user);
		sendUserList();
	}
}

void Channel::removeUser(User* user){
	if(user){
		mUsers.erase(user);
		sendUserList();
	}
	if(mUsers.empty()){
		Mainframe::getInstance()->removeChannel(this);
	}
}

bool Channel::hasUser(User* user){
	return ((mUsers.find(user))!=mUsers.end());
}

UserSet Channel::getUsers(){
	return mUsers;
}

unsigned int Channel::getUserCount() const{
	return mUsers.size();
}

void Channel::broadcast(const std::string& message){
	UserSet::iterator it=mUsers.begin();
	for(;it!=mUsers.end();++it){
		(*it)->session()->send(message);
	}
}

void Channel::broadcast(const std::string& message,const std::string& nickname){
	UserSet::iterator it=mUsers.begin();
	for(;it!=mUsers.end();++it){
		if((*it)->getNickname().compare(nickname)!=0){
			(*it)->session()->send(message);
		}
    }
}

void Channel::sendUserList(){
	UserSet::iterator it=mUsers.begin();
	for(;it!=mUsers.end();++it){
		broadcast(":"+Config::ServerName+" "+ToString(Response::Reply::RPL_NAMREPLY)+" "+(*it)->getNickname()+" = "+mName+" :"+(*it)->getNickname());
	}
	for(it=mUsers.begin();it!=mUsers.end();++it){
		(*it)->session()->sendAsServer(ToString(Response::Reply::RPL_ENDOFNAMES)+" "+(*it)->getNickname()+" "+mName+" :End of /NAMES list.");
	}
}
