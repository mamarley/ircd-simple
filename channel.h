#ifndef CHANNEL_H_INCLUDED
#define CHANNEL_H_INCLUDED

#include <set>
#include <string>
#include "user.h"

typedef std::set<User*> UserSet;

class Channel{
	public:
		Channel(User* creator,const std::string& name);
		~Channel();
		std::string getName() const;
		void addUser(User* user);
		void removeUser(User* user);
		bool hasUser(User* user);
		UserSet getUsers();
		unsigned int getUserCount() const;
		void sendUserList();
		void broadcast(const std::string& message);
		void broadcast(const std::string& message,const std::string& nickname);
	
	private:
		const std::string mName;
		UserSet mUsers;
};

#endif
