#include "defines.h"

std::string Config::EOFMessage="\r\n";
bool Config::Debug=false;

std::string Config::NetworkName="ircd-simple";
std::string Config::ServerName="ircd-simple";
std::string Config::ListenIP="::1";
uint16_t Config::ListenPort=6667;
std::string Config::ServerPassword="";

std::string Config::FormatServerName=":"+Config::ServerName+" ";
