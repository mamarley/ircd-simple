#ifndef DEFINES_H_INCLUDED
#define DEFINES_H_INCLUDED

#include "config.h"

#include <string>
#include <sstream>

template<typename T>
std::string ToString(const T &Value){
	std::ostringstream ostringstream;
	ostringstream<<Value;
	return ostringstream.str();
}

#endif
