#include <iostream>
#include "boost/program_options.hpp"

#include "mainframe.h"

int main(int argc,char *argv[]){
	namespace po=boost::program_options;
	
	po::options_description desc("Options");
	desc.add_options()
	("networkname",po::value<std::string>(&Config::NetworkName),"The IRC network name")
	("servername",po::value<std::string>(&Config::ServerName),"The IRC server name")
	("serverpassword",po::value<std::string>(&Config::ServerPassword),"The IRC server password")
	("listenip",po::value<std::string>(&Config::ListenIP),"The IP address on which to listen")
	("listenport",po::value<uint16_t>(&Config::ListenPort),"The port on which to listen")
	("debug", "Print all IRC statements to console for debugging purposes");
	
	po::variables_map vm;
	try{
		po::store(po::command_line_parser(argc,argv).options(desc).run(),vm);
		po::notify(vm);
		
		if(vm.count("debug")){
			Config::Debug=true;
		}
	}catch(boost::program_options::error& e){ 
		std::cerr<<"Error parsing args!"<<std::endl;
	}
	
	Mainframe* frame=Mainframe::getInstance();
	frame->start();
	
	return 0;
}
