#include "mainframe.h"

Mainframe *Mainframe::mInstance=NULL;

Mainframe* Mainframe::getInstance(){
	if(!mInstance){
		mInstance=new Mainframe();
	}
	return mInstance;
}

Mainframe::Mainframe(){
	
}

Mainframe::~Mainframe(){
	removeAllChannels();
}

void Mainframe::start(){
	boost::asio::io_service io_service;
	Server server(io_service);
	server.start();
	io_service.run();
}

bool Mainframe::doesNicknameExist(const std::string& nickname){
	return ((mUsers.find(nickname))!=mUsers.end());
}

bool Mainframe::addUser(User* user){
	if(doesNicknameExist(user->getNickname())){
		return false;
	}
	mUsers[user->getNickname()]=user;
	return true;
}

bool Mainframe::changeNickname(const std::string& oldNickname,const std::string& newNickname){
	if(doesNicknameExist(newNickname)){
		return false;
	}
	if(!doesNicknameExist(oldNickname)){
		return false;
	}
	User* tmp=mUsers[oldNickname];
	mUsers.erase(oldNickname);
	mUsers[newNickname]=tmp;
	return true;
}

void Mainframe::removeUser(const std::string& nickname){
	mUsers.erase(nickname);
}

User* Mainframe::getUserByNickname(const std::string& nickname){
	if(!doesNicknameExist(nickname)){
		return NULL;
	}
	return mUsers[nickname];
}

bool Mainframe::doesChannelExist(const std::string& name){
	return ((mChannels.find(name))!=mChannels.end());
}

void Mainframe::addChannel(Channel* channel){
	std::string channelName=channel->getName();
	if(!doesChannelExist(channelName)){
		mChannels[channelName]=channel;
	}
}

Channel* Mainframe::getChannelByName(const std::string& name){
	if(!doesChannelExist(name)){
		return NULL;
	}
	return mChannels[name];
}

void Mainframe::removeChannel(Channel* channel){
	mChannels.erase(channel->getName());
}

void Mainframe::removeAllChannels(){
	ChannelMap::iterator it=mChannels.begin();
	while(it!=mChannels.end()){
		delete(it->second);
		it=mChannels.erase(it);
	}
}

ChannelMap Mainframe::getChannels() const{
	return mChannels;
}
