#ifndef MAINFRAME_H_INCLUDED
#define MAINFRAME_H_INCLUDED

#include <map>
#include <string>

#include "server.h"
#include "user.h"
#include "channel.h"

typedef std::map<std::string, User*> UserMap;
typedef std::map<std::string, Channel*> ChannelMap;

class Mainframe{
	public:
		static Mainframe* getInstance();
		void start();
		bool doesNicknameExist(const std::string& nickname);
		bool addUser(User* user);
		void removeUser(const std::string& nickname);
		bool changeNickname(const std::string& oldNickname,const std::string& newNickname);
		User* getUserByNickname(const std::string& nickname);
		bool doesChannelExist(const std::string& name);
		void addChannel(Channel* channel);
		Channel* getChannelByName(const std::string& name);
		void removeChannel(Channel* channel);
		ChannelMap getChannels() const;
	
	private:
		Mainframe();
		~Mainframe();
		Mainframe(const Mainframe&);
		Mainframe& operator=(Mainframe&);
		void removeAllChannels();
		static Mainframe* mInstance;
		UserMap mUsers;
		ChannelMap mChannels;
};

#endif
