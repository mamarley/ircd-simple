#include "parser.h"
#include "server.h"
#include "mainframe.h"

Parser::Parser(){}

void Parser::parse(const std::string& message,User* user){
	if(Config::Debug){
		std::cout<<"IRC RECV:\t"<<message<<std::endl;
	}
	StrVec split;
	boost::split(split,message,boost::is_any_of(" \t"));
	boost::to_upper(split[0]);
	if(split[0]=="NICK"){
		if(split.size()<2){
			user->session()->sendAsServer(ToString(Response::Error::ERR_NONICKNAMEGIVEN)+" "+user->getNickname()+" :No nickname given!");
			return;
		}
		if(split[1][0]=='#'||split[1][0]=='&'||split[1][0]=='!'||split[1][0]=='@'){
			user->session()->sendAsServer(ToString(Response::Error::ERR_ERRONEOUSNICKNAME)+" "+user->getNickname()+" :"+split[1]);
			return;
		}
		if(split[1][0]==':'){
			split[1]=split[1].substr(1);
		}
		user->cmdNick(split[1]);
	}else if(split[0]=="USER"){
		if(split.size()<5){
			user->session()->sendAsServer(ToString(Response::Error::ERR_NEEDMOREPARAMS)+" "+user->getNickname()+" "+split[0]+" :Not enough parameters!");
			return;
		}
		std::string realName=split[4];
		for(unsigned int i=5;i<split.size();++i){
			realName+=" ";
			realName+=split[i];
		}
		if(realName[0]==':'){
			realName=realName.substr(1);
		}
		user->cmdUser(split[1],realName);
	}else if(split[0]=="AWAY"){
		if(split.size()>1){
			user->toggleAway();
		}else{
			user->setAway(false);
		}
	}else if(split[0]=="QUIT"){
		if(split.size()>2){
			std::string message="";
			if(split[1][0]==':'){
				split[1]=split[1].substr(1);
			}
			for(unsigned int i=1;i<split.size();++i){
				message+=split[i];
				if(i<split.size()-1){
					message+=" ";
				}
			}
			user->cmdQuit(message);
		}else{
			user->cmdQuit();
		}
	}else if(split[0]=="JOIN"){
		if(split.size()<2){
			user->session()->sendAsServer(ToString(Response::Error::ERR_NEEDMOREPARAMS)+" "+user->getNickname()+" "+split[0]+" :Not enough parameters!");
			return;
		}
		
		if(!user->isRegistered()){
			user->session()->sendAsServer(ToString(Response::Error::ERR_NOTREGISTERED)+" "+user->getNickname()+" "+split[0]+" :You have not registered!");
			return;
		}
		
		StrVec joinSplit;
		boost::split(joinSplit,split[1],boost::is_any_of(","),boost::token_compress_on);
		
		for(unsigned int i=0;i<joinSplit.size();i++){
			if(joinSplit[i][0]!='#'||joinSplit[i].size()<2){
				return;
			}
			Channel* channel=Mainframe::getInstance()->getChannelByName(joinSplit[i]);
			if(channel){
				if(!channel->hasUser(user)){
					user->cmdJoin(channel);
				}
			}else{
				channel=new Channel(user,joinSplit[i]);
				if(channel){
					user->cmdJoin(channel);
					Mainframe::getInstance()->addChannel(channel);
				}
			}
		}
	}else if(split[0]=="PART"){
		if(split.size()<2){
			user->session()->sendAsServer(ToString(Response::Error::ERR_NEEDMOREPARAMS)+" "+user->getNickname()+" "+split[0]+" :Not enough parameters!");
			return;
		}
		
		if(!user->isRegistered()){
			user->session()->sendAsServer(ToString(Response::Error::ERR_NOTREGISTERED)+" "+user->getNickname()+" "+split[0]+" :You have not registered!");
			return;
		}
		
		Channel* channel=Mainframe::getInstance()->getChannelByName(split[1]);
		if(channel&&channel->hasUser(user)){
			if(split.size()>3){
				std::string message="";
				if(split[2][0]==':'){
					split[2]=split[2].substr(1);
				}
				for(unsigned int i=2;i<split.size();++i){
					message+=split[i];
					if(i<split.size()-1){
						message+=" ";
					}
				}
				user->cmdPart(channel,message);
			}else{
				user->cmdPart(channel);
			}
		}else{
			user->session()->sendAsServer(ToString(Response::Error::ERR_NOTONCHANNEL)+" "+user->getNickname()+" "+split[0]+" :You aren't on that channel!");
			return;
		}
	}else if(split[0]=="LIST"){
		user->session()->sendAsServer(ToString(Response::Reply::RPL_LISTSTART)+" "+user->getNickname()+" Channel :Users Name");
		ChannelMap channels=Mainframe::getInstance()->getChannels();
		ChannelMap::iterator it=channels.begin();
		for(;it!=channels.end();++it){
			user->session()->sendAsServer(ToString(Response::Reply::RPL_LIST)+" "+user->getNickname()+" "+it->first+" "+ToString(it->second->getUserCount())+" :");
		}
		user->session()->sendAsServer(ToString(Response::Reply::RPL_LISTEND)+" "+user->getNickname()+" :End of /LIST");
	}else if((split[0]=="PRIVMSG")||(split[0]=="NOTICE")){
		if(!user->isRegistered()){
			user->session()->sendAsServer(ToString(Response::Error::ERR_NOTREGISTERED)+" "+user->getNickname()+" "+split[0]+" :You have not registered!");
			return;
		}
		
		if(split.size()<3){
			user->session()->sendAsServer(ToString(Response::Error::ERR_NOTEXTTOSEND)+" "+user->getNickname()+" :No text to send!");
			return;
		}
		
		std::string message="";
		for(unsigned int i=2;i<split.size();++i){
			message+=split[i];
			if(i<split.size()-1){
				message+=" ";
			}
		}
		if(split[1][0]=='#'){
			Channel* channel=Mainframe::getInstance()->getChannelByName(split[1]);
			if(channel){
				if(channel->hasUser(user)){
					channel->broadcast(":"+user->getNickname()+" "+split[0]+" "+channel->getName()+" "+message,user->getNickname());
				}else{
					user->session()->sendAsServer(ToString(Response::Error::ERR_CANNOTSENDTOCHAN)+" "+user->getNickname()+" "+split[1]+" :Cannot send to channel.");
				}
			}else{
				user->session()->sendAsServer(ToString(Response::Error::ERR_NOSUCHCHANNEL)+" "+user->getNickname()+" "+split[1]+" :No such channel.");
			}
		}else{
			User* target=Mainframe::getInstance()->getUserByNickname(split[1]);
			if(target){
				target->session()->send(":"+user->getNickname()+" "+split[0]+" "+target->getNickname()+" "+message);
			}else{
				user->session()->sendAsServer(ToString(Response::Error::ERR_NOSUCHNICK)+" "+user->getNickname()+" "+split[1]+" :No such nick.");
			}
		}
	}else if(split[0]=="WHO"){
		if(!user->isRegistered()){
			user->session()->sendAsServer(ToString(Response::Error::ERR_NOTREGISTERED)+" "+user->getNickname()+" "+split[0]+" :You have not registered!");
			return;
		}
		
		if(split.size()<2){
			user->session()->sendAsServer(ToString(Response::Error::ERR_NEEDMOREPARAMS)+" "+user->getNickname()+" "+split[0]+" :Not enough parameters!");
			return;
		}
		
		if(split[1][0]=='#'){
			Channel* channel=Mainframe::getInstance()->getChannelByName(split[1]);
			if(channel){
				UserSet users=channel->getUsers();
				UserSet::iterator it=users.begin();
				for(;it!=users.end();++it){
					user->session()->sendAsServer(ToString(Response::Reply::RPL_WHOREPLY)+" "+user->getNickname()+" "+channel->getName()+" "+(*it)->getUsername()+" "+(*it)->getHost()+" "+Config::ServerName+" "+(*it)->getNickname()+" "+(*it)->getAwayLetter()+" :0 "+(*it)->getRealName());
				}
			}
			user->session()->sendAsServer(ToString(Response::Reply::RPL_ENDOFWHO)+" "+user->getNickname()+" "+split[1]+" :End of /WHO list.");
		}else{
			User* target=Mainframe::getInstance()->getUserByNickname(split[1]);
			if(target){
				user->session()->sendAsServer(ToString(Response::Reply::RPL_WHOREPLY)+" "+user->getNickname()+" * "+target->getUsername()+" "+target->getHost()+" "+Config::ServerName+" "+target->getNickname()+" "+target->getAwayLetter()+" :0 "+target->getRealName());
			}
			user->session()->sendAsServer(ToString(Response::Reply::RPL_ENDOFWHO)+" "+user->getNickname()+" * :End of /WHO list.");
		}
	}else if(split[0]=="PING"){
		if(split.size()>1){
			std::string message="";
			if(split[1][0]==':'){
				split[1]=split[1].substr(1);
			}
			for(unsigned int i=1;i<split.size();++i){
				message+=split[i];
				if(i<split.size()-1){
					message+=" ";
				}
			}
			user->cmdPing(message);
		}else{
			user->cmdPing(Config::NetworkName);
		}
	}else if(split[0]=="PASS"){
		if(split.size()<2){
			return;
		}
		user->cmdPass(split[1]);
	}else{
		user->session()->sendAsServer(ToString(Response::Error::ERR_UNKNOWNCOMMAND)+" "+user->getNickname()+" "+split[0]+" :Unknown command");
	}
}
