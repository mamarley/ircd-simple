#ifndef PARSER_H_INCLUDED
#define PARSER_H_INCLUDED

#include "user.h"
#include <string>

class Parser{
	public:
		static void parse(const std::string& message,User* user);
	
	private:
		Parser();
};

#endif
