#include "server.h"

Server::Server(boost::asio::io_service& io_service)
:	mAcceptor(io_service,tcp::endpoint(boost::asio::ip::address::from_string(Config::ListenIP),Config::ListenPort)),mIoService(io_service){
		mAcceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
		mAcceptor.listen();
}


void Server::start(){
	startAccept();
}

void Server::startAccept(){
	Session::pointer newClient=Session::create(mIoService);
	mAcceptor.async_accept(newClient->getSocket(),boost::bind(&Server::handleAccept,this,newClient,boost::asio::placeholders::error));
}

void Server::handleAccept(Session::pointer newClient, const boost::system::error_code& error){
	if(error){
		std::cerr<<"SERVER ERROR:\t"<<error.message()<<std::endl;
	}else{
		newClient->start();
	}
	startAccept();
}
