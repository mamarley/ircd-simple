#include "session.h"
#include "parser.h"

Session::Session(boost::asio::io_service& io_service)
:	mUser(this),
	mSocket(io_service){}

Session::pointer Session::create(boost::asio::io_service& io_service){
	return Session::pointer(new Session(io_service));
}

void Session::start(){
	read();
}

void Session::close(){
	mSocket.cancel();
	mSocket.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
}

void Session::read(){
	boost::asio::async_read_until(mSocket,mBuffer,Config::EOFMessage,boost::bind(&Session::handleRead,shared_from_this(),boost::asio::placeholders::error,boost::asio::placeholders::bytes_transferred));
}

void Session::handleRead(const boost::system::error_code& error,std::size_t bytes){
	if(error){
		if(error==boost::asio::error::eof){
			mSocket.close();
		}else{
			std::cerr<<"SERVER ERROR:\t"<<error.message()<<" from "<<getIp()<<std::endl;
		}
	}else if(bytes==0){
		std::cerr<<"SERVER ERROR:\tEmpty message sent by "<<getIp()<<std::endl;
	}else{
		std::string message;
		std::istream istream(&mBuffer);
		std::getline(istream,message);
		message=message.substr(0,message.size()-1);
		Parser::parse(message,&mUser);
		read();
	}
}

void Session::send(const std::string& message){
	if(Config::Debug){
		std::cout<<"IRC SEND:\t"<<message<<std::endl;
	}
	try{
		boost::asio::write(mSocket,boost::asio::buffer(message+Config::EOFMessage));
	}catch(std::exception &e){
		std::cout<<"SERVER ERROR:\t"<<e.what()<<" from "<<getIp()<<std::endl;
		// Om nom nom
		// (This happens if a user has quit between the time we decided to send
		// a message and the time we actually send it. It can safely be ignored.)
	}
}

void Session::sendAsUser(const std::string& message){
	send(mUser.getHostmask()+message);
}

void Session::sendAsServer(const std::string& message){
	send(Config::FormatServerName+message);
}

tcp::socket& Session::getSocket(){
	return mSocket;
}

std::string Session::getIp() const{
	boost::system::error_code error;
	return mSocket.remote_endpoint(error).address().to_string();
}
