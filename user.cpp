#include "user.h"
#include "session.h"
#include "mainframe.h"

User::User(Session* mysession)
:	mSession(mysession),
	bSentUser(false),
	bSentNick(false),
	bRegistered(false),
	bProperlyQuit(false),
	bAway(false){}

User::~User(){
	if(!bProperlyQuit){
		ChannelSet::iterator it=mChannels.begin();
		for(;it!=mChannels.end();++it){
			(*it)->broadcast(getHostmask()+"QUIT :lost connection",mNickname);
			(*it)->removeUser(this);
			mChannels.erase((*it));
		}
		Mainframe::getInstance()->removeUser(mNickname);
	}
}

void User::cmdNick(const std::string& nickname){
	if(bSentNick){
		if(Mainframe::getInstance()->changeNickname(mNickname,nickname)){
			ChannelSet::iterator it=mChannels.begin();
			for(;it!=mChannels.end();++it){
				(*it)->broadcast(getHostmask()+"NICK :"+nickname);
			}
			setNickname(nickname);
		}else{
			mSession->sendAsServer(ToString(Response::Error::ERR_NICKCOLLISION)+" "+mNickname+" "+nickname+" :Nickname is already in use.");
		}
	}else{
		if(!Mainframe::getInstance()->doesNicknameExist(nickname)){
			setNickname(nickname);
			Mainframe::getInstance()->addUser(this);
			bSentNick=true;
		}else{
			mSession->sendAsServer(ToString(Response::Error::ERR_NICKNAMEINUSE)+" * "+nickname+" :Nickname is already in use.");
		}
	}
	registerUser();
}

void User::cmdUser(const std::string& username,const std::string& realName){
	if(bSentUser){
		mSession->sendAsServer(ToString(Response::Error::ERR_ALREADYREGISTRED)+" "+mNickname+" You are already registered!");
	}else{
		mUsername=username;
		mHost=Config::ServerName;
		mRealName=realName;
		bSentUser=true;
	}
	registerUser();
}

void User::cmdQuit(const std::string& message){
	ChannelSet::iterator it=mChannels.begin();
	for(;it!=mChannels.end();++it){
		(*it)->broadcast(getHostmask()+"QUIT :"+message,mNickname);
		(*it)->removeUser(this);
		mChannels.erase((*it));
	}
	Mainframe::getInstance()->removeUser(mNickname);
	mSession->close();
	bProperlyQuit=true;
}

void User::cmdQuit(){
	cmdQuit("Client quit");
}

void User::cmdPart(Channel* channel,const std::string& message){
	channel->broadcast(getHostmask()+"PART "+channel->getName()+" :"+message);
	channel->removeUser(this);
	mChannels.erase(channel);
}

void User::cmdPart(Channel* channel){
	cmdPart(channel,"Left the channel");
}

void User::cmdJoin(Channel* channel){
	mSession->sendAsUser("JOIN "+channel->getName());
	channel->broadcast(getHostmask()+"JOIN "+channel->getName(),mNickname);
	mChannels.insert(channel);
	channel->addUser(this);
}

void User::cmdPing(const std::string& message){
	mSession->sendAsServer("PONG "+Config::NetworkName+" :"+message);
}

void User::cmdPass(const std::string& password){
	if(password==Config::ServerPassword){
		bPasswordCorrect=true;
	}else{
		bPasswordCorrect=false;
	}
}

Session* User::session() const{
	return mSession;
}

std::string User::getUsername() const{
	return mUsername;
}

std::string User::getRealName() const{
	return mRealName;
}

std::string User::getNickname() const{
	return mNickname;
}

std::string User::getHost() const{
	return mHost;
}

void User::setNickname(const std::string& nickname){
	mNickname=nickname;
}

void User::setAway(bool away){
	if(away){
		mSession->sendAsServer(ToString(Response::Reply::RPL_NOWAWAY)+" "+mNickname+" :You have been marked as being away");
	}else{
		mSession->sendAsServer(ToString(Response::Reply::RPL_UNAWAY)+" "+mNickname+" :You are no longer marked as being away");
	}
	bAway=away;
}

void User::toggleAway(){
	if(bAway){
		setAway(false);
	}else{
		setAway(true);
	}
}

char User::getAwayLetter(){
	if(bAway){
		return 'G';
	}else{
		return 'H';
	}
}

bool User::isRegistered(){
	return bRegistered;
}

std::string User::getHostmask() const{
	return std::string(":"+mNickname+"!"+mNickname+"@"+mHost+" ");
}

void User::registerUser(){
	if(bSentNick&&bSentUser&&!bRegistered){
		if(bPasswordCorrect||Config::ServerPassword==""){
			bRegistered=true;
			mSession->sendAsServer("001 "+mNickname+" :Welcome to "+Config::NetworkName+" IRC network, "+mNickname);
			mSession->sendAsServer("002 "+mNickname+" :Your host is "+mSession->getIp());
			mSession->sendAsServer("003 "+mNickname+" :This server is "+Config::ServerName);
		}else{
			mSession->sendAsServer(ToString(Response::Error::ERR_PASSWDMISMATCH)+" "+mNickname+" Wrong or no password supplied!");
		}
	}
}
