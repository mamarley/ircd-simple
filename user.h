#ifndef USER_H_INCLUDED
#define USER_H_INCLUDED

#include <boost/algorithm/string.hpp>
#include <string>
#include <vector>
#include <set>

class Session;
class Channel;

typedef std::vector<std::string> StrVec;
typedef std::set<Channel*> ChannelSet;


class User{
	friend class Session;
	public:
		User(Session* mysession);
		~User();
		void cmdNick(const std::string& nickname);
		void cmdUser(const std::string& username,const std::string& realName);
		void cmdQuit(const std::string& message);
		void cmdQuit();
		void cmdJoin(Channel* channel);
		void cmdPart(Channel* channel,const std::string& message);
		void cmdPart(Channel* channel);
		void cmdPing(const std::string& message);
		void cmdPass(const std::string& password);
		Session* session() const;
		std::string getUsername() const;
		std::string getRealName() const;
		std::string getNickname() const;
		std::string getHost() const;
		std::string getHostmask() const;
		void setNickname(const std::string& nickname);
		void setAway(bool away);
		void toggleAway();
		char getAwayLetter();
		bool isRegistered();
	
	private:
		Session* mSession;
		std::string mUsername;
		std::string mRealName;
		std::string mNickname;
		std::string mHost;
		bool bSentUser;
		bool bSentNick;
		bool bRegistered;
		bool bProperlyQuit;
		bool bAway;
		bool bPasswordCorrect;
		ChannelSet mChannels;
		void registerUser();
};

#endif
